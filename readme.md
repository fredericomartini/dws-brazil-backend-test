# DWS Brazil API Test

The project consists of creating a Java backend capable of interacting with third-party services and exposing
a rest API which would be consumed by other platforms such as Android, iOS, WebServers and even other
backend applications. The comps detailing the specific integrations and services are below, we ask you to
follow those and develop the backend as a standalone application, focusing on maintainability, scalability
and performance.

- Firstly, you must consume data from our endpoint and expose it through a REST API with endpoints
that should serve at least, but not limited to, the app wireframes described below;

  - URL: https://iws-brazil-labs-iws-recruiting-bands-staging.iwsbrazil.io/api/full

- Secondly, you must implement a caching layer to optimize your API performance;

### Endpoints API(ISOBAR):
  - /full (All data)
  - /bands (Returns a JSON with all the bands and their information)
  - /bands/:id (Returns a JSON with band information for the specific id parameter as the identifier)
  - /albums (Returns a JSON with all the albums and their information)

# TODO:
- Create a route to list all bands
  - <strike>Should list bands</strike>
  - <strike>Should cache the results</strike>
  - Should order by:
    - name
    - popularity
  - Should search for a band


- Create a route to get a band
  - Should return band info
  - Should return band albuns
  - Should cache the result

- Add api validation (x-api-key)


##



## This project contains

- [TypeScript](https://www.typescriptlang.org/)
- [Express](https://expressjs.com/pt-br/)
- [Nodemon](https://nodemon.io/)
- [Jest](https://jestjs.io/)
- [Docker](https://www.docker.com/)
- [Eslint](https://eslint.org/)
- [Prettier](https://prettier.io/)

<br/><br/>

## Requirements

| Plugin | README |
| ------ | ------ |
| Nodejs | https://nodejs.org/en/ |
| Git | https://git-scm.com/downloads |


<br/><br/>

## Starting
```sh
$ git clone https://gitlab.com/fredericomartini/dws-brazil-backend-test.git
$ cd dws-brazil
```

Install dependencies
```sh
$ npm i
```

Generate .env file based on .env.example
```sh
$ cp .env.example .env
```

<br/><br/>

## Access
| Nome | URL |
| ------ | ------ |
| Application | http://0.0.0.0:3000/v1 |
| Status | http://0.0.0.0:3000/status |
| List Bands | http://0.0.0.0:3000/v1/bands |


<br/><br/>


## Scripts
- `start`: Start application with pm2
- `stop`: Stops the process of pm2
- `dev`: Start application on dev mode (nodemon)
- `debug`: Start application on debug mode (nodemon)
- `lint`: Execute lint in all files with .ts except test ones
- `npm run test`: Execute all unity and functional tests
- `npm run test:unity`: Execute all unity tests
- `npm run test:functional`: Execute all functional tests
- `npm run test:clear`: Clear cache
- `npm run test:watch`: Execute all tests with watch enabled
- `npm run test:coverage`: Execute tests to generate code coverage, available in `coverage/index.html`
- `build"`: Execute the build command, generate all .js files


<br/><br/>

Access localhost:3000

<br/><br/>

## Creation of database using docker
### Requirements

| Plugin | README |
| ------ | ------ |
| Docker | https://docs.docker.com/install |
| Docker Compose | https://docs.docker.com/compose/install |

<br/><br/>


### Start
To start the redis container , (at root folder) use the command:
```sh
$ docker-compose up -d
```

Check if the containers are available:
```sh
$ docker-compose logs redis
```

<br/><br/>

### Access
| Nome | URL | Dados|
| ------ | ------ |------ |
| Redis | http://0.0.0.0:6379 | n/d |
