import * as Joi from 'joi';
import * as supertest from 'supertest';
import * as bodyParser from 'body-parser';
import { Request, Response } from 'express';
import { Controller, Post, Middleware } from '@overnightjs/core';

import validateSchema from '@middlewares/validateSchema';
import errorHandler from '@middlewares/errorHandler';

import SetupServer from '../../../../server';

const schema = Joi.object().keys({
  body: Joi.object({
    idAccount: Joi.string().min(3).required()
  })
});

@Controller('fakeController')
class FakeController {
  @Post('')
  @Middleware(validateSchema(schema))
  private(req: Request, res: Response) {
    return res.json({ account: req.body });
  }
}

describe('Functional tests for validateSchema middleware', () => {
  beforeEach(() => {
    const server = new SetupServer();

    server.getApp().use(bodyParser.json());
    server.addControllers([new FakeController()]);
    server.getApp().use(errorHandler);

    global.request = supertest(server.getApp());
  });

  test('Should return the account when the idAccount has minium 3 characters', async () => {
    const account = { idAccount: '123' };

    const spy = jest.spyOn(schema, 'validate');

    const { body } = await global.request.post('/fakeController').send(account);

    expect(spy).toHaveBeenCalledWith({ body: account }, expect.anything());

    expect(body).toEqual({ account });
  });

  test('Should return error when the idAccount no has minium 3 characters', async () => {
    const account = { idAccount: '1' } as any;

    const spy = jest.spyOn(schema, 'validate');

    const { body } = await global.request.post('/fakeController').send(account);

    expect(spy).toHaveBeenCalledWith({ body: account }, expect.anything());
    expect(body).toEqual({
      success: false,
      message: 'ValidationError',
      code: 400,
      extra: {
        errors: [
          {
            message: '"body.idAccount" length must be at least 3 characters long',
            path: [
              'body',
              'idAccount'
            ],
            type: 'string.min'
          }
        ]
      }
    });
  });
});
