import RedisCache from '@config/database/RedisCache';
import * as IORedis from 'ioredis';
import { createSandbox } from 'sinon';

const sandbox = createSandbox();

beforeAll(async () => {
  sandbox.stub(IORedis.prototype, 'connect').returns(Promise.resolve());
});
const defaultEnv = {
  CACHE: 'true',
  CACHE_HOST: 'my-host',
  CACHE_PORT: 1234,
  CACHE_DB: 3
};

describe('Construct', () => {
  describe('When process.env.CACHE = true', () => {
    beforeEach(async () => {
      sandbox.stub(process, 'env').value(defaultEnv);
    });

    test('Should call getConfig()', () => {
      const spy = jest.spyOn(RedisCache.prototype, 'getConfig');
      const redisDb = new RedisCache();

      expect(redisDb).toBeDefined();

      expect(spy).toHaveBeenCalled();
    });

    describe('getConfig()', () => {
      describe('When process.env.TESTING === true AND CACHE === "true"', () => {
        test('getConfig() Should return TEST config', () => {
          sandbox.stub(process, 'env').value({
            TESTING: true,
            CACHE: 'true',
            CACHE_HOST_TEST: 'testing.com',
            CACHE_PORT_TEST: 5555,
            CACHE_DB_TEST: 55
          });
          const spy = jest.spyOn(RedisCache.prototype, 'getConfig');
          const redisDb = new RedisCache();

          expect(redisDb).toBeDefined();

          expect(spy).toHaveReturnedWith({
            host: 'testing.com',
            port: 5555,
            db: 55,
            connectTimeout: 2 * 1000 // 2s
          });
        });
      });

      describe('When process.env.TESTING !== true AND CACHE === "true"', () => {
        test('getConfig() Should return default config', () => {
          sandbox.stub(process, 'env').value(defaultEnv);
          const spy = jest.spyOn(RedisCache.prototype, 'getConfig');
          const redisDb = new RedisCache();

          expect(redisDb).toBeDefined();

          expect(spy).toHaveReturnedWith({
            host: 'my-host',
            port: 1234,
            db: 3,
            connectTimeout: 2 * 1000 // 2s
          });
        });
      });
    });

    test('getDb() Should return instanceOf Redis', () => {
      const redisDb = new RedisCache();

      expect(redisDb.getDb()).toBeInstanceOf(jest.requireActual('ioredis'));
    });
  });

  describe('When process.env.CACHE = false', () => {
    beforeEach(async () => {
      sandbox.stub(process, 'env').value({
        CACHE: 'false',
        CACHE_HOST: 'my-host',
        CACHE_PORT: 1234,
        CACHE_DB: 3
      });
    });

    test('Should NOT call getConfig()', () => {
      const spy = jest.spyOn(RedisCache.prototype, 'getConfig');
      const redisDb = new RedisCache();

      expect(redisDb).toBeDefined();

      expect(spy).not.toHaveBeenCalled();
    });

    test('getDb() Should return undefined', () => {
      const redisDb = new RedisCache();

      expect(redisDb.getDb()).toBeUndefined();
    });
  });
});
