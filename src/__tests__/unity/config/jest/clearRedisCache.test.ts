import RedisCache from '@config/database/RedisCache';
import clearRedisCache from '@config/jest/clearRedisCache';

let spyFlushall: jest.SpyInstance;
let spyGetDb: jest.SpyInstance;

beforeEach(async () => {
  spyFlushall = jest.fn();
  spyGetDb = jest.spyOn(RedisCache.prototype, 'getDb').mockImplementation(() => ({
    flushall: spyFlushall
  }) as any);
});

describe('clearRedisCache()', () => {
  test('Should call RedisCache.getDb()', async () => {
    await clearRedisCache();
    expect(spyGetDb).toHaveBeenCalled();
  });

  test('Should call RedisCache.flushall()', async () => {
    await clearRedisCache();
    expect(spyFlushall).toHaveBeenCalled();
  });

  test('Should not call Redis.flushall when no Redis', async () => {
    spyGetDb.mockImplementation(() => undefined);
    await clearRedisCache();
    expect(spyFlushall).not.toHaveBeenCalled();
  });
});
