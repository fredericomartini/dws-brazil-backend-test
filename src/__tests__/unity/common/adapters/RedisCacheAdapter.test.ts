import RedisCacheAdapter from '@adapters/RedisCacheAdapter';

const key = 'my-super-key';
let cacheAdapter: RedisCacheAdapter;
let spyExists: jest.SpyInstance;
let spyGet: jest.SpyInstance;
let spySetex: jest.SpyInstance;
let spyUnlink: jest.SpyInstance;

beforeAll(async () => {
  spyExists = jest.fn();
  spyGet = jest.fn();
  spySetex = jest.fn();
  spyUnlink = jest.fn();

  cacheAdapter = new RedisCacheAdapter({
    db: jest.fn(),
    getDb: () => ({
      exists: spyExists,
      get: spyGet,
      setex: spySetex,
      unlink: spyUnlink
    })
  } as any);
});

describe('Constructor', () => {
  describe('exists()', () => {
    test('Shoul call IORedis.exists() with key', async () => {
      await cacheAdapter.exists(key);

      expect(spyExists).toHaveBeenNthCalledWith(1, key);
    });

    test('Should return true when key exists', async () => {
      spyExists.mockResolvedValue(1);
      const exists = await cacheAdapter.exists(key);

      expect(exists).toBeTrue();
    });

    test('Should return false when key NOT exists', async () => {
      spyExists.mockResolvedValue(0);
      const exists = await cacheAdapter.exists(key);

      expect(exists).toBeFalse();
    });
  });

  describe('get()', () => {
    test('Shoul call IORedis.get() with key', async () => {
      spyGet.mockResolvedValue('');

      await cacheAdapter.get(key);
      expect(spyGet).toHaveBeenNthCalledWith(1, key);
    });

    test('Should call JSON.parse() with string from Cache when get an existent key', async () => {
      spyGet.mockResolvedValue(JSON.stringify('my-data'));
      const spy = jest.spyOn(JSON, 'parse');

      await cacheAdapter.get(key);

      expect(spy).toHaveBeenNthCalledWith(1, JSON.stringify('my-data'));
    });

    test('Should return "my-data" when get an existent key', async () => {
      spyGet.mockResolvedValue(JSON.stringify('my-data'));
      const response = await cacheAdapter.get(key);

      expect(response).toBe('my-data');
    });

    test('Should return null when key NOT exists', async () => {
      spyGet.mockResolvedValue(null);
      const data = await cacheAdapter.get(key);

      expect(data).toBeNull();
    });
  });

  describe('setex()', () => {
    test('Shoul call IORedis.setex() with key', async () => {
      spySetex.mockResolvedValue(1);

      await cacheAdapter.set(key, 'my-data', 2000);

      expect(spySetex).toHaveBeenNthCalledWith(1,
        key,
        2000,
        expect.any(String));
    });

    test('Shoul call JSON.stringify()', async () => {
      spySetex.mockResolvedValue(1);
      const spyJSON = jest.spyOn(JSON, 'stringify');
      const data = {
        my_key: 123,
        other: ['1', 2]
      };

      await cacheAdapter.set(key, data, 2000);

      expect(spyJSON).toHaveBeenNthCalledWith(1, data);
      expect(spySetex).toHaveBeenNthCalledWith(1,
        key,
        2000,
        JSON.stringify(data));
    });

    test('Should return true when save with success', async () => {
      spySetex.mockResolvedValue('OK');
      const response = await cacheAdapter.set(key, 'my-data', 2000);

      expect(response).toBeTrue();
    });

    test('Should return false when not save', async () => {
      spySetex.mockResolvedValue(null);
      const data = await cacheAdapter.set(key, 'data', 2000);

      expect(data).toBeFalse();
    });
  });

  describe('del()', () => {
    test('Shoul call IORedis.unlink() with key', async () => {
      spyUnlink.mockResolvedValue(1);

      await cacheAdapter.del(key);
      expect(spyUnlink).toHaveBeenNthCalledWith(1, key);
    });
  });
});
