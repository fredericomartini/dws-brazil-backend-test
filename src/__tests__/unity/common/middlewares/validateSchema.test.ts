import * as httpMocks from 'node-mocks-http';
import * as Joi from 'joi';
import validateSchema, { getJoiConfig, parseErrors } from '@middlewares/validateSchema';
import { Request } from 'express';

describe('Tests for validateSchema middleware', () => {
  const headers = { 'x-api-key': '' };
  let req: Request;
  const res = httpMocks.createResponse();
  const next = jest.fn();

  const schemaWithBody = Joi.object().keys({
    body: Joi.object().keys({
      name: Joi.string().min(3).required()
    })
  });

  const schemaWithParams = Joi.object().keys({
    params: Joi.object().keys({
      id: Joi.string().uuid().required()
    })
  });

  const schemaWithQuery = Joi.object().keys({
    query: Joi.object().keys({
      skip: Joi.number().optional().default(0),
      take: Joi.number().optional().default(10)
    })
  });

  beforeEach(() => {
    req = httpMocks.createRequest({ headers });
  });

  describe('getJoiConfig()', () => {
    test('Should have default properties', () => {
      expect(getJoiConfig()).toEqual({
        abortEarly: false,
        allowUnknown: false
      });
    });
  });

  describe('parseErrors()', () => {
    test('Should remove key context from errors array', () => {
      const errors = [
        { name: 'my error 1', message: 'Error 1', context: { name: 'my-context 1' } },
        { name: 'my error 2', message: 'Error 2', context: { name: 'my-context 2' } }
      ];
      const noErrors = parseErrors(errors as any);

      const [error1, error2] = noErrors;

      expect(error1).toEqual({ name: 'my error 1', message: 'Error 1' });
      expect(error2).toEqual({ name: 'my error 2', message: 'Error 2' });
    });
  });

  describe('validateSchema', () => {
    test('Should call schema.validate with req.params', () => {
      const params = { id1: '123', id2: '456' } as any;

      req.params = params;

      const spy = jest.spyOn(schemaWithParams, 'validate');

      validateSchema(schemaWithParams)(req, res, next);

      expect(spy).toHaveBeenCalledWith(expect.objectContaining({
        params
      }), expect.anything());
    });

    test('Should call schema.validate with req.query', () => {
      const query = { search: 'my-search', skip: 0, take: 10 } as any;

      req.query = query;

      const spy = jest.spyOn(schemaWithQuery, 'validate');

      validateSchema(schemaWithQuery)(req, res, next);

      expect(spy).toHaveBeenCalledWith(expect.objectContaining({
        query
      }), expect.anything());
    });

    test('Should call schema.validate with req.body', () => {
      const body = { name: 'John' };

      req.body = body;

      const spy = jest.spyOn(schemaWithBody, 'validate');

      validateSchema(schemaWithBody)(req, res, next);

      expect(spy).toHaveBeenCalledWith(expect.objectContaining({
        body
      }), expect.anything());
    });
  });

  describe('When valid request', () => {
    test('Should call next() when req.params is valid', () => {
      req.params = { id: 'ef2c7e98-2b3c-11eb-a4c8-0242ac130003' };

      validateSchema(schemaWithParams)(req, res, next);

      expect(next).toHaveBeenCalledWith();
    });

    test('Should call next() when req.query is valid', () => {
      req.query = { skip: 1, take: 10 } as any;

      validateSchema(schemaWithQuery)(req, res, next);

      expect(next).toHaveBeenCalledWith();
    });

    test('Should call next() when req.body is valid', () => {
      req.body = { name: 'John' };

      validateSchema(schemaWithBody)(req, res, next);

      expect(next).toHaveBeenCalledWith();
    });
  });

  describe('When invalid request', () => {
    test('Should call next() with ValidationError when req.params is invalid', () => {
      req.params = { name: 'some value' };

      validateSchema(schemaWithParams)(req, res, next);

      expect(next).toHaveBeenCalledWith(new Error('ValidationError'));
    });

    test('Should call next() with ValidationError when req.query is invalid', () => {
      req.query = { skip: 'some value' };

      validateSchema(schemaWithParams)(req, res, next);

      expect(next).toHaveBeenCalledWith(new Error('ValidationError'));
    });

    test('Should call next() with ValidationError when req.body is invalid', () => {
      req.body = { name: '' } as any;

      validateSchema(schemaWithBody)(req, res, next);

      expect(next).toHaveBeenCalledWith(new Error('ValidationError'));
    });
  });
});
