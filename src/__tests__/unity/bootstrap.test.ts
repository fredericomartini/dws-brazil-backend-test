import * as monitoring from '@config/monitoring';
import { bootstrap } from '../../bootstrap';

let spyStartMonitoring: jest.SpyInstance;

beforeEach(() => {
  spyStartMonitoring = jest.spyOn(monitoring, 'default').mockImplementation(() => jest.fn());
});

describe('Bootstrap unity test', () => {
  test('Should call startMonitoring()', () => {
    bootstrap();
    expect(spyStartMonitoring).toHaveBeenCalled();
  });
});
