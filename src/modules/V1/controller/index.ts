
import {
  Controller,
  ChildControllers
} from '@overnightjs/core';

import { BandController } from '@modules/Band';

import Container, { Service } from 'typedi';

@Controller('v1')
@ChildControllers([
  Container.get(BandController)
])
@Service()
export default class V1 {}
