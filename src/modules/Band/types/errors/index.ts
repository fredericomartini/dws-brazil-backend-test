/* eslint-disable max-classes-per-file */
import CustomException from '@helpers/error/CustomException';

export class GetBandError extends CustomException {
  constructor(extra?: any) {
    super({
      message: 'GetBandError',
      code: 422
    }, extra);
  }
}

export class BandNotFoundError extends CustomException {
  constructor(extra?: any) {
    super({
      message: 'BandNotFoundError',
      code: 404
    }, extra);
  }
}

export class ListBandError extends CustomException {
  constructor(extra?: any) {
    super({
      message: 'ListBandError',
      code: 422
    }, extra);
  }
}
