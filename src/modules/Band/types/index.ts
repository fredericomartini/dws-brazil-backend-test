export * from './errors';

export enum OrderBand {
  'name',
  'popularity'
}

export type ListBandInput = {
  search?: string | undefined,
  order?: keyof typeof OrderBand
}
