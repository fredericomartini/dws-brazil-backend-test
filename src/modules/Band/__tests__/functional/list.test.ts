
describe('Should have defined route', () => {
  test('GET "/v1/bands"', async () => {
    const response = await global.request.get('/v1/bands');

    expect(response.status).not.toBeOneOf([401, 404]);
  });
});

describe('Should return data', () => {
  test('Should return success and array of bands', async () => {
    const { status, body: { success, data } } = await global.request.get('/v1/bands');

    expect(status).toBe(200);
    expect(success).toBeTrue();
    expect(data).toBeDefined();
  });
});
