import IsobarApi from '@modules/Isobar/IsobarApi';
import Container from 'typedi';
import { Cache as CacheAdapter } from '@typeDefs/cache';
import Cache from '@services/Cache';
import * as logger from '@helpers/logger';
import BandService from '@modules/Band/Band.service';
import * as classTransformer from 'class-transformer';
import Band from '@modules/Band/Band.entity';

let service: BandService;

class CacheFactory implements CacheAdapter {
  active: boolean;

  exists: any = jest.fn();

  get: any = jest.fn();

  set: any = jest.fn();

  del: any = jest.fn();
}

beforeAll(() => {
  Container.set(Cache, new CacheFactory());
  service = new BandService(
    Container.get(IsobarApi),
    Container.get(Cache)
  );
});
const search = 'my album';
const order = 'name';

describe('When no errors', () => {
  describe('Caching', () => {
    const key = `bands:${JSON.stringify({ search, order })}`;

    describe('Should return data from cache when exists', () => {
      let spyExists: jest.SpyInstance;
      let spyGet: jest.SpyInstance;

      beforeEach(async () => {
        spyExists = jest.spyOn(Container.get(Cache), 'exists').mockResolvedValue(true);
        spyGet = jest.spyOn(Container.get(Cache), 'get');
      });

      test('Should call Cache.exists(), then Cache.get() with key', async () => {
        await service.list({ search, order });

        expect(spyExists).toHaveBeenNthCalledWith(1, key);

        expect(spyGet).toHaveBeenNthCalledWith(1, key);
      });

      test('Should call plainToClass with Band and json data', async () => {
        const spy = jest.spyOn(classTransformer, 'plainToClass');
        const bands = [{ name: 'Pink Floyd' }];

        jest.spyOn(Container.get(Cache), 'exists').mockResolvedValue(false);
        jest.spyOn(IsobarApi.prototype, 'list').mockResolvedValue(bands as any);

        await service.list({ search, order });

        expect(spy).toHaveBeenNthCalledWith(1, Band, bands);
      });

      test('Should NOT call IsobarApi.list()', async () => {
        const spy = jest.spyOn(IsobarApi.prototype, 'list').mockResolvedValue('' as any);

        await service.list({});

        expect(spy).not.toHaveBeenCalled();
      });
    });

    describe('Should set data to cache', () => {
      test('Should call Cache.set(), with key and bands', async () => {
        const bands = [] as any;

        jest.spyOn(Container.get(Cache), 'exists').mockResolvedValue(false);
        jest.spyOn(IsobarApi.prototype, 'list').mockResolvedValue(bands);
        const spySet = jest.spyOn(Container.get(Cache), 'set');

        await service.list({ search, order });

        expect(spySet).toHaveBeenNthCalledWith(1, key, bands);
      });
    });
  });

  describe('Should call IsobarApi', () => {
    test('Should call IsobarApi.list()', async () => {
      const spy = jest.spyOn(IsobarApi.prototype, 'list').mockResolvedValue('' as any);

      await service.list({});

      expect(spy).toHaveBeenCalledTimes(1);
    });

    test('Should call plainToClass with Band and json data', async () => {
      const spy = jest.spyOn(classTransformer, 'plainToClass');
      const bands = [{ name: 'Pink Floyd' }];

      jest.spyOn(IsobarApi.prototype, 'list').mockResolvedValue(bands as any);

      await service.list({ search, order });

      expect(spy).toHaveBeenNthCalledWith(1, Band, bands);
    });
  });
});

describe('When some error', () => {
  beforeEach(async () => {
    jest.spyOn(IsobarApi.prototype, 'list').mockRejectedValue('');
  });

  test('Should call logError()', async () => {
    const spy = jest.spyOn(logger, 'logError');

    await expect(() => service.list({})).rejects.toThrow();

    expect(spy).toHaveBeenCalled();
  });

  test('Should throw ListBandError', async () => {
    await expect(() => service.list({})).rejects.toThrow('ListBandError');
  });
});
