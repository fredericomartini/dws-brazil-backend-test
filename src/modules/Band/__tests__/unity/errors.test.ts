import {
  GetBandError,
  ListBandError,
  BandNotFoundError
} from '@modules/Band';

describe('BandNotFoundError', () => {
  test('Should return message BandNotFoundError', () => {
    const error = new BandNotFoundError();

    expect(error.message).toEqual('BandNotFoundError');
  });

  test('Should return code 404', () => {
    const error = new BandNotFoundError();

    expect(error.code).toEqual(404);
  });
});

describe('GetBandError', () => {
  test('Should return message GetBandError', () => {
    const error = new GetBandError();

    expect(error.message).toEqual('GetBandError');
  });

  test('Should return code 422', () => {
    const error = new GetBandError();

    expect(error.code).toEqual(422);
  });
});

describe('ListBandError', () => {
  test('Should return message ListBandError', () => {
    const error = new ListBandError();

    expect(error.message).toEqual('ListBandError');
  });

  test('Should return code 422', () => {
    const error = new ListBandError();

    expect(error.code).toEqual(422);
  });
});
