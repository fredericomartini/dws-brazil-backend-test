import * as Joi from 'joi';

export const schemaGet = Joi.object().keys({
  params: Joi.object({
    id_band: Joi.string().required()
  }).required()
});

export const schemaList = Joi.object().keys({
  query: Joi.object().keys({
    search: Joi.string(),
    order: Joi.string().valid('name', 'popularity')
  })
});
