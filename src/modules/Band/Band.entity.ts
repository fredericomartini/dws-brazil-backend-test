export default class Band {
  name: string;

  image: string;

  genre: string;

  biography: string;

  numPlays: number;

  albums: string[];

  id: string;
}
