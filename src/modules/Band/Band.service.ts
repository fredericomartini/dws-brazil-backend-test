import { Service } from 'typedi';
import { logError } from '@helpers/logger';
import Cache from '@services/Cache';
import { plainToClass } from 'class-transformer';
import { ListBandError, ListBandInput } from './types';
import IsobarApi from '../Isobar/IsobarApi';
import Band from './Band.entity';

@Service()
export default class BandService {
  constructor(
    readonly isobarApi: IsobarApi,
    readonly cache: Cache
  ) {}

  async list({
    search,
    order = 'name'
  }: ListBandInput): Promise<any> {
    try {
      const key = `bands:${JSON.stringify({ search, order })}`;

      // Verify in cache
      if (await this.cache.exists(key)) {
        return plainToClass(Band, await this.cache.get(key));
      }

      const bands = await this.isobarApi.list();

      // Store in cache
      await this.cache.set(key, bands);

      return plainToClass(Band, bands);
    } catch (error) {
      logError(error);

      throw new ListBandError();
    }
  }
}
