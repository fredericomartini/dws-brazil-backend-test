import { Request, Response, NextFunction } from 'express';
import { Service } from 'typedi';
import {
  Controller,
  Middleware,
  Get
} from '@overnightjs/core';
import validateSchema from '@middlewares/validateSchema';
import { BandService, schemaList } from '.';
import { OrderBand } from './types';

@Controller('bands')
@Service()
export default class BandController {
  constructor(private readonly service: BandService) {
  }

  @Get('')
  @Middleware(validateSchema(schemaList))
  async list(req: Request, res: Response, next: NextFunction) {
    try {
      return res.status(200).json({
        success: true,
        data: await this.service.list({
          search: req.query.search as string,
          order: req.query.order as keyof typeof OrderBand
        })
      });
    } catch (error) {
      return next(error);
    }
  }
}
