import BaseApi from '@services/BaseApi';
import { AxiosResponse } from 'axios';
import { plainToClass } from 'class-transformer';
import { Service } from 'typedi';
import Band from '../Band/Band.entity';

@Service()
export default class IsobarApi extends BaseApi {
  constructor() {
    const { ISOBAR_URL } = process.env;

    const headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json'
    };

    super(ISOBAR_URL!, '', false, headers);
  }

  async list(): Promise<Band[]> {
    const {
      data: bands
    }: AxiosResponse< Band[] > = await this.service.get('/bands');

    return plainToClass(Band, bands);
  }
}
