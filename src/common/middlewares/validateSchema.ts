import { Request, Response, NextFunction } from 'express';
import * as Joi from 'joi';
import { ValidationError } from '@typeDefs/errors';

export const getJoiConfig = (): Joi.ValidationOptions => ({
  abortEarly: false,
  allowUnknown: false
});

export const parseErrors = (errors: Joi.ValidationErrorItem[]) => errors.map(({
  context, ...error
}) => (error));

export const validateSchema = (schema: Joi.Schema) => ({
  params,
  query,
  body
}: Request, _: Response, next: NextFunction) => {
  try {
    const data = {} as any;

    if (Object.keys(params).length) {
      data.params = params;
    }
    if (Object.keys(query).length) {
      data.query = query;
    }

    if (Object.keys(body).length) {
      data.body = body;
    }

    const { error } = schema.validate(data, getJoiConfig());

    if (error) {
      throw new ValidationError({ errors: parseErrors(error.details) });
    }

    next();
  } catch (error) {
    next(error);
  }
};

export default validateSchema;
