/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
// import { Connection, createConnection, MigrationExecutor } from 'typeorm';

require('ts-node/register');
require('tsconfig-paths/register');

// export const rollbackMigrations = async (connection: Connection) => {
//   console.log('\nRunning back all executed migrations...');
//   const queryRunner = connection.createQueryRunner();

//   const migrationExecutor = new MigrationExecutor(connection, queryRunner);
//   const migrations = await migrationExecutor.getExecutedMigrations();

//   /* eslint-disable no-await-in-loop */
//   for (let i = 0; i < migrations.length; i += 1) {
//     await migrationExecutor.undoLastMigration();
//   }
//   console.log('Rolled back all executed migrations');
// };

// export const runMigrations = async (connection: Connection) => {
//   console.log('Running all migrations...');
//   await connection.runMigrations();
//   console.log('Rolled all migrations');
// };

export default async () => {
  // only run migrations on integration tests
  // TODO: verificar problema ao rodar unity tests e alguma entity não tiver tabela criada ainda..
  // if (process.env.TESTING_TYPE !== 'UNITY') {
  // const connection = await createConnection();

  // await rollbackMigrations(connection);

  // await runMigrations(connection);

  // await connection.close();
  // }
};
