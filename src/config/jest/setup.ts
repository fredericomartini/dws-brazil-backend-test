import 'jest-extended';
import * as supertest from 'supertest';
// import * as typeorm from 'typeorm';
import Container from 'typedi';
import { SetupServer } from '../../server';
// import clearDatabase from './clearDatabase';

let server: SetupServer;

beforeAll(async () => {
  server = Container.get(SetupServer);
  await server.init();

  global.request = supertest(server.getApp());

  // jest.spyOn(typeorm, 'createConnections').mockImplementation(jest.fn());

  // await clearDatabase();
});

afterAll(async () => {
  // only clean database on FUNCTIONAL tests
  if (process.env.TESTING_TYPE === 'FUNCTIONAL') {
    // await clearDatabase();
  }

  // await server.closeDatabase();
});

beforeEach(async () => {
  jest.resetAllMocks();
  jest.clearAllMocks();
  jest.restoreAllMocks();
});
