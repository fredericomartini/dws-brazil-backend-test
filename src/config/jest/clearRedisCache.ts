import Container from 'typedi';
import RedisCache from '@config/database/RedisCache';

export default async () => {
  const Redis = Container.get(RedisCache).getDb();

  if (!Redis) {
    return;
  }

  return Redis.flushall();
};
