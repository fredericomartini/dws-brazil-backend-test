import startLogger from '@config/monitoring/logger';

export default () => {
  startLogger();
};
