import * as IORedis from 'ioredis';
import { Service } from 'typedi';

@Service()
export default class RedisCache {
  private readonly db: IORedis.Redis;

  constructor() {
    if (process.env.CACHE === 'true') {
      this.db = new IORedis(this.getConfig());
    }
  }

  getConfig() {
    const connectTimeout = 2 * 1000; // 2s

    if (process.env.TESTING) {
      return {
        host: process.env.CACHE_HOST_TEST as string,
        port: Number(process.env.CACHE_PORT_TEST),
        db: Number(process.env.CACHE_DB_TEST),
        connectTimeout
      };
    }

    return {
      host: process.env.CACHE_HOST as string,
      port: Number(process.env.CACHE_PORT),
      db: Number(process.env.CACHE_DB),
      connectTimeout
    };
  }

  getDb() {
    return this.db;
  }
}
