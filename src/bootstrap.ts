import 'reflect-metadata';
import 'dotenv-safe/config';
import './config/setAliases';
import startMonitoring from '@config/monitoring';

export const bootstrap = () => {
  startMonitoring();
};

export default bootstrap();
